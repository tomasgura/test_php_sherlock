<?php

require_once __DIR__ . '/../../lib/SherlockParser.php';
require_once __DIR__ . '/../../lib/SherlockArray.php';
require_once __DIR__ . '/../../lib/NProfiller.php';

use PHPUnit\Framework\TestCase;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SherlockParserTest
 *
 * @author tomas.gura
 */
class SherlockParserTest extends TestCase
{

	//put your code here

	private $sp;

	public function setUp() {
		$this->sp = new SherlockParser();
	}
	
	public function fixProvider() {
		return [
			['a','a'],
			['ahoj','ahoj'],
			['1',1],
		];
	}
	
	/**
	 * @dataProvider fixProvider
	 */
	public function testFixNumber($num,$fixedNum) {
		$fn = $this->sp->fixNumber($num);
		$this->assertEquals($fn,$fixedNum);
	}
	
	
	public function addArrayToArrayProvider() {
		$ret = [
			[	
				[], 
				["N" => ["24413"]],
				["N" => ["24413"]] 
			],

			[	
				["N" => ["24413"]],
				[], 
				["N" => ["24413"]] 
			],
			
			[	
				["N" => ["24413"]], 
				["N" => ["2222"]],
				["N" => ["24413","2222"]] 
			],

			[	
				["N" => ["24413","2121"]], 
				["N" => ["2222"]],
				["N" => ["24413","2121", "2222"]] 
			],
			
			[
				["N" => ["24413","2121"]], 
				["P" => ["2222"]],
				[
					"N" => ["24413","2121"],
					"P" => ["2222"],
				],
			],
			
			[
				["N" => ["24413","2121"]], 
				["P" => ["2222"]],
				[
					"P" => ["2222"],
					"N" => ["24413","2121"],
				],
			],
			[
				[ "M" => [
					"title" => [
						0 => [
							"w" => 0,
							"m" => 0,
							"e" => 1,
							0 => "SexChat.cz - Živý erotický videochat"
							]
						]
					]
				],
				[ "M" => [
					"subtitle" => [
						0 => [
							"w" => 1,
							"m" => 1,
							"e" => 1,
							0 => "Zkuste <best>sex</best> po webce"
							]
						]
					]
				],
				[ "M" => [
					"title" => [
						0 => [
							"w" => 0,
							"m" => 0,
							"e" => 1,
							0 => "SexChat.cz - Živý erotický videochat"
							]
						],
					"subtitle" => [
						0 => [
							"w" => 1,
							"m" => 1,
							"e" => 1,
							0 => "Zkuste <best>sex</best> po webce"
							]
						]
					]
				],
			]
		];

		// slucuji dve stejna pole - musim dostat to same.
		$metaArray = ["M" => [
						"sectionname" => [[
							"w" => 0,
							"m" => 0,
							"e" => 1,
							0 => " Erotika ",
						]]
					]
				];
		
		$metaArray2 = ["M" => [
						"sectionname" => [[
							"w" => 0,
							"m" => 0,
							"e" => 1,
							0 => " Erotika2 ",
						]]
					]
				];
		
		$metaarraySpojene = ["M" => [
						"sectionname" => [  
							[
								"w" => 0,
								"m" => 0,
								"e" => 1,
								0 => " Erotika ",
							],
							[
								"w" => 0,
								"m" => 0,
								"e" => 1,
								0 => " Erotika ",
							],
						]
			
					]
				];
				
		array_push($ret, [ $metaArray,$metaArray,$metaArray ]);
		// posledni plati
		array_push($ret, [ $metaArray,$metaArray2,$metaArray2 ]);
		array_push($ret, [ $metaArray2,$metaArray,$metaArray ]);
		return $ret;
	}


	/**
	 * @dataProvider addArrayToArrayProvider
	 * @group test
	 * @group xdebug
	 */
	public function testAddArrayToArray (array $input,array $add, array $excepted) {
		$mixed = $this->sp->addArrayToArray($add,$input,1);
		$p1 = var_export($mixed,true);
		$p2 = var_export($excepted,true);
		$msg = "Pole nesedi - ".PHP_EOL." aktualni ".PHP_EOL.$p1.PHP_EOL." ocekavane ".PHP_EOL.$p2.PHP_EOL;
		$this->assertEquals($excepted,$mixed,$msg);
	}

	
	

	public function parseOneLineProvider() {
		return [
			[["N24413"], ["N" => ["24413"]],0,0],
			[["Dmain"], ["D" => ["main"]],0,0],
			[["Ustatic:k313054:i530:g6" ],["U" => ["static:k313054:i530:g6"]],1,0],			
			[["W3139826 1 0 1 0 1100000001242ffffffffffffffff"], ["W" => ["3139826 1 0 1 0 1100000001242ffffffffffffffff"]], 0,0],
			[['0{"regions":[],"districts":[],"towns":[],"sections":[],"tags":[]}'],
				["0" => ["{\"regions\":[],\"districts\":[],\"towns\":[],\"sections\":[],\"tags\":[]}"]],
				0,0
			],
			[[""],[],0,0],
			//parsujem meta tag
			[["M<url>nngmain:139873</url>"] ,
				[
					"M" => [
						"url" => [["nngmain:139873"]]
					]
				],
				1,0
			],


			
			
			[["M<url>static:k313054:i530:g6</url>"] ,
				[
					"M" => [
						"url" => [["static:k313054:i530:g6"]]
					]
				],
				1,0
			],
			
			
			[["M<meta w=0 m=0 e=1><text> Pikantní web pro chlapy - Pikant.cz </text>"] ,
				[
					"M" => [
						"text" => [[
							0 => " Pikantní web pro chlapy - Pikant.cz ",
							'w' => 0,
							"m" => 0,
							"e" => 1
							]]
					]
				],
				1,0
			],
			
			
			[["M<meta w=0 m=0 e=1><text> Pikantní web pro chlapy - Pikant.cz </text>"] ,
				[
					"M" => [
						"text" => [
							0 => " Pikantní web pro chlapy - Pikant.cz ",
							'w' => 0,
							"m" => 0,
							"e" => 1
							]
					]
				],
				1,1
			],
			
			
			/**
					[M] => Array
						(
							[text] => Array
								(
									[w] => 0
									[m] => 0
									[e] => 1
									[0] =>  Pikantní web pro chlapy - Pikant.cz 
								)

						)

			 * 
			 */
			
			
			
			// neparsujem  meta tag
			[["M<url>nngmain:139873</url>" ],
				[
				"M" => ["<url>nngmain:139873</url>"]
				
				],
				0,0
			],
			
			// neparsujem  meta tag
			[["M<meta w=f m=1 e=1><title> <best>Svet</best> <best>testu</best> <best>a</best> <best>restu</best> </title>"],
				[
				"M" => ["<meta w=f m=1 e=1><title> <best>Svet</best> <best>testu</best> <best>a</best> <best>restu</best> </title>"]
				
				],
				0,0
				
			],
			
			
			// parsujem  meta tag
			[["M<meta w=f m=1 e=1><title> <best>Svet</best> <best>testu</best> <best>a</best> <best>restu</best> </title>"],
				[
					"M" => [
						"title" => [[
							0 => " <best>Svet</best> <best>testu</best> <best>a</best> <best>restu</best> ",
							'w' => 'f',
							'm' => 1,
							'e' => 1
						]]
							
					]
				],
				1,0
				
			],
			
			// parsujem  meta tag - nezalezi na poradi v poli
			[["M<meta w=f m=1 e=1><title> <best>Svet</best> <best>testu</best> <best>a</best> <best>restu</best> </title>"],
				[
					"M" => [
						"title" => [[
							'w' => 'f',
							'm' => 1,
							'e' => 1,
							0 => " <best>Svet</best> <best>testu</best> <best>a</best> <best>restu</best> ",
						]]
							
					]
				],
				1,0
				
			],
			// zdroj najisto
			[["M<meta w=0 m=0 e=0> <block c=0-75 l=67><promotext> Provozujeme internetový obchod zaměřený na prodej erotického zboží, </promotext></block>"],
				[
					"M" => [
						"promotext" => [[
							'w' => 0,
							'm' => 0,
							'e' => 0,
							'begin' => 0,
							'end' => 75,
							0 => " Provozujeme internetový obchod zaměřený na prodej erotického zboží, ",
						]]
							
					]
				],
				1,0
				
			],

			//zdroj najisto ale jako search jestli jsem spravne pochopil single_meta - při single_meta=1 nepřidáváme begin a end
			[["M<meta w=0 m=0 e=0> <block c=0-75 l=67><promotext> Provozujeme internetový obchod zaměřený na prodej erotického zboží, </promotext></block>"],
				[
					"M" => [
						"promotext" => [
							'w' => 0,
							'm' => 0,
							'e' => 0,
							0 => " Provozujeme internetový obchod zaměřený na prodej erotického zboží, ",
						]
							
					]
				],
				1,1
				
			],
			
			// zdroj search
			[["M<meta w=0 m=0 e=0> <block c=0-72 l=67><promotext> Jsme luxusní a diskrétní night club v Praze jen nedaleko od centra. </promotext></block>"],
				[
					"M" => [
						"promotext" => [
							'w' => 0,
							'm' => 0,
							'e' => 0,
							0 => " Jsme luxusní a diskrétní night club v Praze jen nedaleko od centra. ",
						]
							
					]
				],
				1,1
				
			],



			[["M<meta w=0 m=0 e=1><sectionname> Erotické podniky </sectionname>"],
				[
					"M" => [
						"sectionname" => [
							"w" => 0,
							"m" => 0,
							"e" => 1,
							0 => " Erotické podniky ",
						]
							
					]
				],
				1,1
			],

			
			
//			],
//			["(D", '"(D":[{'],
//			[")", '}]'],
//			["", '']
		];
	}

	/**
	 * @dataProvider parseOneLineProvider
	 * @depends testAddArrayToArray
	 * @group test
	 */
	public function testParseOneLine(array $input,array $output,bool $parseMeta,int $metaSingle) {
		$pole = $this->sp->parseOneLine($input,$parseMeta,$metaSingle);
		$this->assertEquals($output,$pole,"Meta single ".$metaSingle);
	}
	
	
	
	
	
	public function parseMetaTagOneLineProvider() {
		return [
			
			[["M<url>static:k313054:i530:g6</url>"],
				[
					"M" => [
						"url" => [  0 => "static:k313054:i530:g6"],
					]
				],
				1,1
			],

			
			[["M<url>static:k313054:i530:g6</url>"],
				[
					"M" => [
						"url" => [ [ "static:k313054:i530:g6"]],
					]
				],
				1,0
			],
			
			// overeni ze testParsePartialSherlockResponseToArrayMetaTags nedela chybu v parsovani jedne radky
			[["M<meta w=0 m=0 e=1><sectionname> Erotika </sectionname>"],
				[
					"M" => [
						"sectionname" => [
								"w" => 0,
								"m" => 0,
								"e" => 1,
								0 => " Erotika ",
						]
					]
				],
				1,1
			],

			[["M<meta w=0 m=0 e=1><sectionname> Erotika </sectionname>"],
				[
					"M" => [
						"sectionname" => [
							0 => [
								"w" => 0,
								"m" => 0,
								"e" => 1,
								0 => " Erotika ",
						]
					]
					]
				],
				1,0
			],
			
		];
	}
	
	/**
	 * @dataProvider parseMetaTagOneLineProvider
	 * @depends testAddArrayToArray
	 * @group test
	 */
	public function testParseMetaTagOneLine(array $input,array $output,bool $parseMeta,int $metaSingle) {
		$pole = $this->sp->parseOneLine($input,$parseMeta,$metaSingle);
		$this->assertEquals($output,$pole,"Meta single ".$metaSingle);
	}

	/**
	 */
	public function testGetStartAndEndFromBlockTag() {
		$lines = ["M<meta w=0 m=0 e=0> <block c=0-75 l=67><promotext>  zboží, </promotext></block>"];
		$pars = ["<block c=0-75 l=67>","c=0-75 l=67"];
		$ret = ["begin" => 0, "end" => 75];
		$begEnd = $this->sp->getStartAndEndFromBlockTag($lines, $pars);
		$this->assertEquals($ret, $begEnd);
		
		
		
		$lines = [
			"M<meta w=1 m=1 e=1> <block c=18-68 l=46><promotext> videochat,  </promotext></block>",
			"M<block c=101-126 l=23><promotext> nezávazný <best>sex</best> po webce. </promotext></block>"
			];
		$pars = ["<block c=18-68 l=46>","c=18-68 l=46"];
		$ret = ["begin" => 18, "end" => 126];
		$begEnd = $this->sp->getStartAndEndFromBlockTag($lines, $pars);
		$this->assertEquals($ret, $begEnd);
	}
	
	
	public function parsePartialSherlockResponseToArrayMetaTagsDataProvider() {
		$inp = "M<meta w=0 m=0 e=1><sectionname> Erotika </sectionname>".PHP_EOL;
		$inp .= $inp;
		
		$out = [
					"M" => [
						"sectionname" => [
							"w" => 0,
							"m" => 0,
							"e" => 1,
							0 => " Erotika ",
						]
					]
				];
		
		$ret = array();
		array_push($ret, [$inp,$out]);
		return $ret;
		
	}
	
	/**
	 * @dataProvider parsePartialSherlockResponseToArrayMetaTagsDataProvider
	 * @depends testAddArrayToArray
	 * @group test
	 */
	public function xtestParsePartialSherlockResponseToArrayMetaTags($inp,$expOut) {
		$raw = array();
		
		$fp = fopen("php://memory", 'r+');
		fputs($fp, $inp);
		rewind($fp);
		$out = $this->sp->parsePartialSherlockResponseToArray($fp, $raw, true, 1);
		fclose($fp);
		
		$p1 = var_export($out,true);
		$p2 = var_export($expOut,true);
		$msg = "Pole nesedi - ".PHP_EOL." aktualni ".PHP_EOL.$p1.PHP_EOL." ocekavane ".PHP_EOL.$p2.PHP_EOL;
		
		
		$this->assertEquals($expOut, $out,$msg);

	}

	public function testPartialSherlockResponceForEmptyString() {
		$response = "";
		$raw = array();
		$fp = fopen("php://memory", 'r+');
		fputs($fp, $response);
		rewind($fp);
		$respArray = [];
		$ra = $this->sp->parsePartialSherlockResponseToArray($fp,$raw,false,0);
		fclose($fp);
		$this->assertEquals($respArray,$ra);
	}
	

	public function testParseOneLineSherslockResponse() {
		$response = "Dmain";
		$respArray = [
			"HEAD" => [ 
				"D" => ["main"],
			],
			"CARDS" => [],
			"FOOT" => [],
			
		];
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}
	
	public function testParseOneLineSherslockResponseWithRaw() {
		$response = "Dmain";
		$respArray = [
			"RAW" => [ [  
				"Dmain"  ]
			],
			"HEAD" => [ 
				"D" => ["main"],
			],
			"CARDS" => [],
			"FOOT" => [],
			
		];
		$ra = $this->sp->parseSherlockResponseToArray($response,1,false,0);
		$this->assertEquals($respArray,$ra);
	}
	
	
	public function testSimpleParseSherslockResponse() {
		$response = "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420";
		
		$respArray = [
			"HEAD" => [   
			"D" => ["main"],
			"N" => ["24421"],
			"I" => ["24420"],
			],
			"CARDS" => [],
			"FOOT" => [],
			
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}
	
	public function testBlockParseSherslockResponse() {
		$response = "";
		$response .= "(D".PHP_EOL;
		$response .= "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420".PHP_EOL;
		$response .= ")".PHP_EOL;
		
		$respArray = [
			"HEAD" => [
				"(D" => [
				[    
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"],
				]
				]
			],
			"CARDS" => [],
			"FOOT" => [],
			
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}
	
	public function testMultiBlockParseSherslockResponse() {
		$response = "";
		$response .= "(D".PHP_EOL;
		$response .= "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "N392".PHP_EOL;
		$response .= "m70".PHP_EOL;
		$response .= "V4.0-devel-nng-main".PHP_EOL;
		$response .= "I59d6c54400007bb0".PHP_EOL;
		$response .= "@main-localhost".PHP_EOL;
		$response .= "(M".PHP_EOL;
		$response .= "c0".PHP_EOL;
		$response .= "Rmain".PHP_EOL;
		$response .= ")".PHP_EOL;
		
		$respArray = [
			"HEAD" => [
				"(D" => [ [   
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"], ]
				],
				"N" => ['392'],
				"m" => ['70'],
				"V" => ['4.0-devel-nng-main'],
				"I" => ['59d6c54400007bb0'],
				"@" => ['main-localhost'],
				"(M" => [ [  
					"c" => ['0'],
					"R" => ['main'], ]
				],
			],
			"CARDS" => [],
			"FOOT" => [],
			
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}
	

	
	public function testParseSherslockResponseWithFoot() {
		$response = "";
		$response .= "(D".PHP_EOL;
		$response .= "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "N392".PHP_EOL;
		$response .= "m70".PHP_EOL;
		$response .= "V4.0-devel-nng-main".PHP_EOL;
		$response .= "I59d6c54400007bb0".PHP_EOL;
		$response .= "@main-localhost".PHP_EOL;
		$response .= "(M".PHP_EOL;
		$response .= "c0".PHP_EOL;
		$response .= "Rmain".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "".PHP_EOL;
		
		//foot
		$response .= "+".PHP_EOL;
		$response .= "Tanal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32".PHP_EOL;
		$response .= "t9".PHP_EOL;
		$response .= "+++".PHP_EOL;
		
		$respArray = [
			"HEAD" => [
				"(D" => [  [ 
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"], ]
				],
				"N" => ['392'],
				"m" => ['70'],
				"V" => ['4.0-devel-nng-main'],
				"I" => ['59d6c54400007bb0'],
				"@" => ['main-localhost'],
				"(M" => [  [ 
					"c" => ['0'],
					"R" => ['main'], ]
				],
			],
			"CARDS" => [],
			"FOOT" => [
				"T" => ["anal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32"],
				"t" => ["9"],
			]
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}
	


	public function testParseSherslockResponseWithFootAndCards() {
		$response = "";
		$response .= "(D".PHP_EOL;
		$response .= "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "N392".PHP_EOL;
		$response .= "m70".PHP_EOL;
		$response .= "V4.0-devel-nng-main".PHP_EOL;
		$response .= "I59d6c54400007bb0".PHP_EOL;
		$response .= "@main-localhost".PHP_EOL;
		$response .= "(M".PHP_EOL;
		$response .= "c0".PHP_EOL;
		$response .= "Rmain".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "".PHP_EOL;
		
		// cards
		$response .= "Bmain".PHP_EOL;
		$response .= "O00001171".PHP_EOL;
		$response .= "w64".PHP_EOL;
		$response .= "Tde0b267ada8d".PHP_EOL;
		$response .= "".PHP_EOL;
		
		// foot
		$response .= "+".PHP_EOL;
		$response .= "Tanal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32".PHP_EOL;
		$response .= "t9".PHP_EOL;
		$response .= "+++".PHP_EOL;
		
		$respArray = [
			"HEAD" => [
				"(D" => [  [  
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"], ]
				],
				"N" => ['392'],
				"m" => ['70'],
				"V" => ['4.0-devel-nng-main'],
				"I" => ['59d6c54400007bb0'],
				"@" => ['main-localhost'],
				"(M" => [   [  
					"c" => ['0'],
					"R" => ['main'], ]
				],
			],
			"CARDS" => [
				[   
					"B" => ["main"],
					"O" => ["00001171"],
					"w" => ["64"],
					"T" => ["de0b267ada8d"],
				]
			],
			"FOOT" => [
				"T" => ["anal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32"],
				"t" => ["9"],
			]
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}




	public function testParseSherslockResponseWithFootAndManyCards() {
		$response = "";
		$response .= "(D".PHP_EOL;
		$response .= "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "N392".PHP_EOL;
		$response .= "m70".PHP_EOL;
		$response .= "V4.0-devel-nng-main".PHP_EOL;
		$response .= "I59d6c54400007bb0".PHP_EOL;
		$response .= "@main-localhost".PHP_EOL;
		$response .= "(M".PHP_EOL;
		$response .= "c0".PHP_EOL;
		$response .= "Rmain".PHP_EOL;
		$response .= ")".PHP_EOL;
		$response .= "".PHP_EOL;
		
		// card0
		$response .= "Bmain".PHP_EOL;
		$response .= "O00001171".PHP_EOL;
		$response .= "w64".PHP_EOL;
		$response .= "Tde0b267ada8d".PHP_EOL;
		$response .= "".PHP_EOL;
		
		// card1
		$response .= "Bmain".PHP_EOL;
		$response .= "O000051ac".PHP_EOL;
		$response .= "w64".PHP_EOL;
		$response .= "T555175460833".PHP_EOL;
		$response .= "".PHP_EOL;
		
		// foot
		$response .= "+".PHP_EOL;
		$response .= "Tanal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32".PHP_EOL;
		$response .= "t9".PHP_EOL;
		$response .= "+++".PHP_EOL;
		
		$respArray = [
			"HEAD" => [
				"(D" => [   [  
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"], ]
				],
				"N" => ['392'],
				"m" => ['70'],
				"V" => ['4.0-devel-nng-main'],
				"I" => ['59d6c54400007bb0'],
				"@" => ['main-localhost'],
				"(M" => [  [   
					"c" => ['0'],
					"R" => ['main'], ]
				],
			],
			"CARDS" => [
				[   
					"B" => ["main"],
					"O" => ["00001171"],
					"w" => ["64"],
					"T" => ["de0b267ada8d"],
				],
				[   
					"B" => ["main"],
					"O" => ["000051ac"],
					"w" => ["64"],
					"T" => ["555175460833"],
				]
				
			],
			"FOOT" => [
				"T" => ["anal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32"],
				"t" => ["9"],
			]
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($respArray,$ra);
	}


	public function testParseSherslockResponseWithFootAndManyCardsWithRawData() {
		$rawData = array();
		
		$responseHead = "";
		$responseHead .= "(D".PHP_EOL;
		$responseHead .= "Dmain".PHP_EOL;
		$responseHead .= "N24421".PHP_EOL;
		$responseHead .= "I24420".PHP_EOL;
		$responseHead .= ")".PHP_EOL;
		$responseHead .= "N392".PHP_EOL;
		$responseHead .= "m70".PHP_EOL;
		$responseHead .= "V4.0-devel-nng-main".PHP_EOL;
		$responseHead .= "I59d6c54400007bb0".PHP_EOL;
		$responseHead .= "@main-localhost".PHP_EOL;
		$responseHead .= "(M".PHP_EOL;
		$responseHead .= "c0".PHP_EOL;
		$responseHead .= "Rmain".PHP_EOL;
		$responseHead .= ")";

		$rawData[] = explode(PHP_EOL, $responseHead);
		// card0
		$responseCard0 = "";
		$responseCard0 .= "Bmain".PHP_EOL;
		$responseCard0 .= "O00001171".PHP_EOL;
		$responseCard0 .= "w64".PHP_EOL;
		$responseCard0 .= "Tde0b267ada8d";
		
		$rawData[] = explode(PHP_EOL, $responseCard0);
		
		// card1
		$responseCard1 = "";
		$responseCard1 .= "Bmain".PHP_EOL;
		$responseCard1 .= "O000051ac".PHP_EOL;
		$responseCard1 .= "w64".PHP_EOL;
		$responseCard1 .= "T555175460833";
		
		$rawData[] = explode(PHP_EOL, $responseCard1);
		
		// foot
		$responseFoot = "";
		$responseFoot .= "+".PHP_EOL;
		$responseFoot .= "Tanal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32".PHP_EOL;
		$responseFoot .= "t9".PHP_EOL;
		$responseFoot .= "+++";
		$rawData[] = explode(PHP_EOL, $responseFoot);
		
		$response = $responseHead.PHP_EOL."".PHP_EOL.$responseCard0.PHP_EOL."".PHP_EOL.$responseCard1.PHP_EOL."".PHP_EOL.$responseFoot;
		
		$respArray = [
			"RAW" => $rawData,
			"HEAD" => [
				"(D" => [   [  
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"], ]
				],
				"N" => ['392'],
				"m" => ['70'],
				"V" => ['4.0-devel-nng-main'],
				"I" => ['59d6c54400007bb0'],
				"@" => ['main-localhost'],
				"(M" => [  [   
					"c" => ['0'],
					"R" => ['main'], ]
				],
			],
			"CARDS" => [
				[   
					"B" => ["main"],
					"O" => ["00001171"],
					"w" => ["64"],
					"T" => ["de0b267ada8d"],
				],
				[   
					"B" => ["main"],
					"O" => ["000051ac"],
					"w" => ["64"],
					"T" => ["555175460833"],
				]
				
			],
			"FOOT" => [
				"T" => ["anal=0.002267 reff=0.000615 refs=0.001119 resf=0.000133 resu=0.004734 wrds:2 phrs:0 near:0 chns:8 chKB:32"],
				"t" => ["9"],
			]
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,1,false,0);
		$this->assertEquals($respArray,$ra);
	}



	public function testErrorCode() {
		$msg = "+000 OK";
		$response = $msg.PHP_EOL;
		$response .= "(D".PHP_EOL;
		$response .= "Dmain".PHP_EOL;
		$response .= "N24421".PHP_EOL;
		$response .= "I24420".PHP_EOL;
		$response .= ")".PHP_EOL;
		
		$respArray = [
			"HEAD" => [
				"(D" => [
				[    
					"D" => ["main"],
					"N" => ["24421"],
					"I" => ["24420"],
				]
				]
			],
			"CARDS" => [],
			"FOOT" => [],
			
		];
		
		$ra = $this->sp->parseSherlockResponseToArray($response,0,false,0);
		$this->assertEquals($msg,$this->sp->errorCode);
	}
	
	
}
