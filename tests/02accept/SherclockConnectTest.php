<?php

require_once __DIR__ . '/../../lib/php-sherlock.php';

use PHPUnit\Framework\TestCase;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SherlockConnecttTest extends TestCase
{
	private $sherlock_url = "adp-dev-sherlock.dev.chservices.cz";
	private $sherlock_port = 12100;
	
	
	public function testConnectToSherclock() {
		$query = 'MUXSS "sec" SHOW 1..25 SNAME "kolo"';
		$reply = "";
		get_sherlock_data($this->sherlock_url, $this->sherlock_port, $query, $reply);
		$this->assertTrue( $reply != "" );
	}
	
	public function testMainFuncParseSherlockReply() {
		$query = 'MUXSS "sec" SHOW 1..25 SNAME "kolo"';
		$reply = array();
		parse_sherlock_reply($this->sherlock_url, $this->sherlock_port, $query, $flags = 1, $reply);
		$this->assertTrue(TRUE);

	}
}