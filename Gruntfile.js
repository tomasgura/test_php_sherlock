module.exports = function(grunt) {

	// Autoload all Grunt tasks
	require('matchdep').filterAll('grunt-*').forEach(grunt.loadNpmTasks);
	require('phplint').gruntPlugin(grunt);

	// Project configuration.  
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		phpcs: {
			test: {
				
				src: ['lib/*.php', 'tests/**/*.php'],
				
				options: {
					bin: 'vendor/bin/phpcs',
					standard: 'phpcs.xml',
				}
			},
			fix: {
				src: ['lib/**/*.php','tests/**/*.php'],
				options: {
					bin: 'vendor/bin/phpcbf',
					standard: 'phpcs.xml',
				}
			}
		},
		phplint: {
			options: {
				limit: 10,
				stdout: true,
				stderr: true,
				useCache: true, // Defaults to false 
			 },
			files: ['lib/*.php','tests/**/*.php']
		},		
		'phpunit-runner': {
			all: {
				options: {
				phpunit: 'vendor/bin/phpunit'
			  }
			},
			unit: {
				options: {
					phpunit: 'vendor/bin/phpunit'
				},
				files: {
					testFiles: 'tests/01unit/'
				}
			},
			accept: {
				options: {
					phpunit: 'vendor/bin/phpunit'
				},
				files: {
					testFiles: 'tests/02integ/'
				}
			},
			full: {
				options: {
					phpunit: 'vendor/bin/phpunit'
				},
				files: {
					testFiles: 'tests/'
				}
			},
			coverage:{
					options: {
						phpunit: 'vendor/bin/phpunit',
						coverageHtml: 'tests/coverage/',
						configuration: 'tests/phpunit.xml',
						excludeGroup: 'skip'
					},
					files: {
						testFiles: 'tests/'
					}
                               
			},
			coverageunit:{
				options: {
					phpunit: 'vendor/bin/phpunit',
					coverageHtml: 'tests-phpu/coverage/',
					configuration: 'tests-phpu/phpunit.xml'
				},
				files: {
					testFiles: 'tests-phpu/unit/'
				}
			},
			coverageinteg:{
				options: {
					phpunit: 'vendor/bin/phpunit',
					coverageHtml: 'tests-phpu/coverage/',
					configuration: 'tests-phpu/phpunit.xml'
				},
				files: {
					testFiles: 'tests-phpu/integ/'
				}
			}
		},

		availabletasks: {
			tasks: {
			}
		},		

		watch: {
			scripts: {
			files: ['lib/*.php','include/**/*.php','tests-phpu/**/*.php'],
			tasks: ['phpunit-runner:unit','phpcs:test'],
			options: {
				spawn: false
				}
			}
		},
		uglify: {
		  options: {
			banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
		  },
		  build: {
			src: 'src/<%= pkg.name %>.js',
			dest: 'build/<%= pkg.name %>.min.js'
		  }
		}
	  });

	grunt.registerTask('tasks', ['availabletasks']);
	

};