<?php

class NProfiler {

	private static $timers = array();
	private static $enabled = false;

	public static function enable() {
		static::$enabled = true;
	}

	public static function disable() {
		static::$enabled = false;
	}

	public static function start($name, $info = null) {
		if (!static::$enabled) return;

		$trace = debug_backtrace();
		//\NLogger::dump($trace);
		$file = null;
		if (isset($trace[0])) {
			$file = preg_replace('!^.*?(\/[^\/]+\/[^\/]+)$!','...$1',$trace[0]['file']).' #'.$trace[0]['line'];
		}
		
		$timerId = microtime();// mt_srand();
		$timer = new Timer($name, $info, $file);
		$timer->start = microtime(true);
		static::$timers[$name][$timerId] = $timer;
		return $timerId;
	}

	public static function stop($name, $timerId, $info = null) {
		if (!static::$enabled) return;

		if (isset(static::$timers[$name][$timerId])) {
			static::$timers[$name][$timerId]->end = microtime(true);
			static::$timers[$name][$timerId]->info = $info;
			$delta = round(static::$timers[$name][$timerId]->end - static::$timers[$name][$timerId]->start, 4);
			static::$timers[$name][$timerId]->delta = $delta;
		}
	}

	public static function toHtml() {
		if (!static::$enabled) return '';
		$cells='';
		foreach (static::$timers as $message) {
			$cells.=$message->toHtml();
		}
		$out = '<html><head><style>#pfl { background: #CFEBFF ; font-size: 12px; padding: 10px; margin: 10px 0px 10px 0px; border: 1px solid gray;} #pfl th {font-weight: bold;} #pfl td, #pfl th {border-right: 1px solid black; border-bottom: 1px solid black; padding: 2px;} #pfl table {border-left: 1px solid black;border-top: 1px solid black; background: white;}</style></head><body><div id="pfl"><div><strong>Profiler:</strong></div>';
		$out.= '<table border="0"><tr><th>name</th><th>time</th><th>start</th><th>stop</th><th>file</th><th>info</th></tr>';
		$out.= $cells;
		$out.='</table></div></body></html>';
		return $out;
	}

	public static function asArray() {
		if (!static::$enabled) return array();
		return static::$timers;
	}

	public static function toString() {
		if (!static::$enabled) return 'NProfiller is currently disabled, no stats available.';
		$str = '';

		foreach (static::$timers as $timerKey => $timer) {
			$pomStr = "";
			$totalDelta = 0;
			foreach ($timer as $timerItem) {
				if (isset($timerItem)) {
					$pomStr.= "\t".$timerItem->toString()."\n";
					$totalDelta += $timerItem->delta;
				}
			}
			$str .= "$timerKey $totalDelta s \n$pomStr \n";
		}
		
		return "\nNProfiler stats:\n".$str;
	}
	
	public function reset() {
		empty(self::$timers);
		self::$timers = array();
	}

}

class Timer {

	protected $name;
	public $info;
	public $start;
	public $end;
	public $file;
	public $delta;

	public function __construct($name, $info = null, $file = null) {
		$this->info = $info;
		$this->name = $name;
		$this->file = $file;
	}

	public function toString() {

		$str = str_pad($this->name, 30).' ';

		if (!isset($this->start)) {
			$str.= str_pad('Start not set', 47);
		} elseif (!isset($this->end)) {
			$str.= str_pad('End not set', 47);
		} else {
			$str.= str_pad($this->delta.'s', 10).'[ '.str_pad($this->start,15)." > ".str_pad($this->end, 15).' ]';
		}

		$str.= isset($this->file)? ' '.$this->file : '';
		$str.= isset($this->info)? ' i: '.$this->info : '';

		return $str;
	}

	public function toHtml() {

		$str = '<tr><td>'.$this->name.'</td>';

		if (!isset($this->start)) {
			$str.= '<td colspan="3">Start not set</td>';
		} elseif (!isset($this->end)) {
			$str.= '<td colspan="3">End not set</td>';
		} else {
			$str.= '<td>'.$this->delta.'s'.'</td><td>'.$this->start."</td><td>".$this->end.'</td>';
		}

		$str.= (isset($this->file)? '<td>'.$this->file : '<td>').'</td>';
		$str.= (isset($this->info)? '<td>'.$this->info : '<td>').'</td>';

		return $str.'</tr>';
	}
	
}

