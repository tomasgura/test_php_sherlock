<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SherlockArray
 *
 * @author tomas.gura
 */
class SherlockArray
{
	private $rawResponseArray;
	
	
	/**
	 * Řadky, které příjdou z sherlocka
	 */
	public $lines;
	
	
	public $linesArray;
	
	/**
	 * Pole head z vstupního jsonu
	 */
	public $head;
	
	
	/**
	 * Pole cards z vstupního jsonu
	 */
	public $cards;
	
	
	/**
	 * Pole cards v stupního jsonu
	 */
	public $foot;
	
	
	
	/**
	 * Odpověd bez části raw
	 */
	public $jsonResponse;
	
	public function __construct($json) {
		$this->rawResponseArray = json_decode($json,true);
		$this->parse();
		$this->linesArray = array();
	}
	
	private function parse() {
		$this->lines = "";
		$rawBlocks = count($this->rawResponseArray['RAW']);
		foreach ($this->rawResponseArray['RAW'] as $keyBlock => $blok) {
			foreach ($blok as $key => $val) {
				$this->lines .= $val.PHP_EOL;
				$this->linesArray[] = $val;
			}
			if ($keyBlock < $rawBlocks - 1) {
				$this->lines .= PHP_EOL;
				$this->linesArray[] = $val;
			}
		}
		$this->head = $this->rawResponseArray['HEAD'];
		$this->cards = $this->rawResponseArray['CARDS'];
		$this->foot = $this->rawResponseArray['FOOT'];
		
		$responseArray = array();
		$responseArray['HEAD'] = $this->head;
		$responseArray['CARDS'] = $this->cards;
		$responseArray['FOOT'] = $this->foot;
		$this->jsonResponse = json_encode($responseArray);
		
	}
}
