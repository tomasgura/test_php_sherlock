<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SherlockParser
 *
 * @author tomas.gura
 */
class SherlockParser
{
	//put your code here
	
	private $emptyArray = [];
	public $debug = 0;
	public $debugArray = array();
	public $errorCode = "";


	public function fixNumber($number) {
		$ret = $number;
		if (is_numeric($number)) {
			$ret = (int) $number;
		}
		return $ret;
	}
	
	private function getStartAndEndFromLine(string $line) {
		$ret = array();
		$odDo = array();
		$params = array();
		$blockArray = explode(' ', $line);
		foreach ($blockArray as $blockOneParam) {
			if ($blockOneParam != "") {
				preg_match("~(\S)=(\S*)~", $blockOneParam,$params);
				if (count($params) > 1) {
					if ($params[1] === 'c') {
						preg_match("~(\d*)-(\d*)~", $params[2], $odDo);
						$begin = $odDo[1];
						$end = $odDo[2];
					}
				}
			}
		}
		$ret['begin'] = (int) $begin;
		$ret['end'] = (int) $end;
		return $ret;
		
	}
	
	public function getStartAndEndFromBlockTag(array $lines, array $blockParams) {
		$blockParamsLine = array();
		$ret = $this->getStartAndEndFromLine($blockParams[1]);
		if (count($lines) > 1) {

			if (preg_match("~<block (.*?)>~",$lines[1],$blockParamsLine)) { 
				$r2 = $this->getStartAndEndFromLine($blockParamsLine[1]);
				$ret['end'] = max($ret['end'],$r2['end']);
			}
		}
		return $ret;

	}
	
	public function parseTagM(array $lines,int $metaSingle) {
		// m -> <meta tag> je tam jednou a to ten posledni / prepise predchozi.
		
		$match = array();
		$matchTag = array();
		$blockParams = array();
		$line = $lines[0];
		
		if (preg_match("~^M<meta (.*?)>(.*)~",$line,$match)) {
				$tagLine = $match[2];
				preg_match("~<(\S*?)>(.*)(</\\1>)~", $tagLine, $matchTag);
				$tagArray = [];
				if ($metaSingle === 0) {
					if (preg_match("~<block (.*?)>~",$tagLine,$blockParams)) {
						$beginEnd = $this->getStartAndEndFromBlockTag($lines,$blockParams);
						$tagArray['begin'] = $beginEnd['begin'];
						$tagArray['end'] = $beginEnd['end'];
					}
				}
				
				$tag = $matchTag[1];
				$metaParams = $match[1];
				$metaArray = explode(' ', $metaParams);
				foreach ($metaArray as $metaItem) {
					$a = array();
					preg_match("~(\S)=(\S)~", $metaItem,$a);
					$tagArray[$a[1]] = $this->fixNumber($a[2]);
				}
				$tagArray[] = $matchTag[2];
				if (count($lines) > 1) {
					preg_match("~<$tag>(.*)</$tag>~", $lines[1],$line2content);
					//var_dump($line2content);
					$tagArray[] = $line2content[1];
				}
				
				//echo "Meta single .".$metaSingle.PHP_EOL;
				if ($metaSingle === 0) {
					$tagArrayVPoli = [$tagArray];
				}
				
				if ($metaSingle === 1) {
					$tagArrayVPoli = $tagArray;
				}
				
				$ret = [ $tag => $tagArrayVPoli];
			
		} else {
			if (preg_match("~<(\S*?)>(.*?)</(.*?)>~", $line, $match)) {
				$obsah = $match[2];
				$tag = $match[1];
				$obsahVPoli = [$obsah];
				
				if ($metaSingle === 1 && $tag === 'url') {
					$obsahVPoli = $obsah;
				}
				$ret = [ $tag => [$obsahVPoli] ];
			}
		}
		return $ret;
	}
	
	/**
	 * Zpracuje jednu radku odpovedi scherlocka
	 * @param array $lines radka odpovedi
	 * @param bool $parseMetaData zpracovat meta data / tag M
	 * @param int $metaSingle z format dat meta - muze mit hodnoty 0 a 1 (0 pro najisto, 1 search)
	 * @return array odpoved v poli
	 */
	public function parseOneLine(array $lines,bool $parseMetaData, int $metaSingle) {
		$ret = array();
		if (count($lines)) {
			if ($lines[0] !== "") {
				$letter = $lines[0][0];
				// tag M je meta a musí se parsovat zvlášť.
				if ($parseMetaData && $letter === 'M') {
					$values = $this->parseTagM($lines,$metaSingle);
				} else {
					$values = array (substr($lines[0],1,strlen($lines[0]) - 1));
				}
				$ret[$letter] = $values;
			}
		}
		return $ret;
	}
	
	
	private function mergeArrays(array $add, array $to) {
		foreach ($add as $key => $value) {
			if (!isset($to[$key])) {
				$to[$key] = $value;
			} else {
				$to[$key] = array_merge_recursive($to[$key],$value);
			}
		}
		return $to;
	}
	
	private function mergeMetaArraysSingleMeta(array $add, array $to) {
		if (isset( $add["M"])) {
			foreach ($add["M"] as $key => $value) {
				$to["M"][$key] = $value;
			}
		} else {
			$to = $this->mergeArrays($add, $to);
		}
		return $to;
	}
	
	
	
	/**
	 * Přidá pole do pole
	 * @param array $add Description
	 * @param array $to Description
	 * @param int $metaSingle default 0 
	 * @return array Description
	 * 
	 */
	public function addArrayToArray(array $add, array $to,int $metaSingle = 0) {
		if ($this->debug) {
			array_push($this->debugArray, array ( __METHOD__ => $add ));
		}
		if (isset($add["M"]) || isset($to["M"])) {
			if ($metaSingle === 1) {
				$ret = $this->mergeMetaArraysSingleMeta ($add, $to);
			} else {
//				$ret = $this->mergeMetaArrays($add, $to);
				$ret = $this->mergeArrays($add,$to);
			}
		} else {
			$ret = $this->mergeArrays($add,$to);
		}
		return $ret;
	}
	
	/**
		Převede blok odpovědi sherlocka na pole
	 * 	 */
	public function parsePartialSherlockResponseToArray($stream,array &$rawResponse, bool $parseMetaTag, int $metaSingle) {
		$fncId = NProfiler::start(__FUNCTION__);
		$ret = array();
		$nextLine = false;
		$metaSingleA2A = $metaSingle;
		if (!$parseMetaTag)
			$metaSingleA2A = 0;
//		$lineNumber = 0;
		do {
			$streamLine = fgets($stream);
			$line = str_replace(array("\n","\r"), '', $streamLine);
			$firstLetter = '';
			if ($line !== "") {
				$rawResponse[] = $line;
				$firstLetter = $line[0];
				switch ($firstLetter) {
					case '(':
						$group = $line[1];
						$responseArray = $this->parsePartialSherlockResponseToArray($stream,$rawResponse,$parseMetaTag,$metaSingle);
						$groupArray = [$firstLetter.$group => [$responseArray] ];
						$ret = $this->addArrayToArray($groupArray, $ret,$metaSingleA2A);
						$nextLine = true;
						break;
					case ')':
						$nextLine = false;
						break;
					case '+':
						$nextLine = true;
						break;
					default:
						$lines = array();
						$lines[] = $line;
						if (preg_match("~^M<meta~", $line)) {
							$pos = ftell($stream);
							$streamLine2 = fgets($stream);
							if (preg_match("~^M<block~", $streamLine2)) {
								$line2 = str_replace(array("\n","\r"), '', $streamLine2);
								$lines[] = $line2;
								$rawResponse[] = $line2;
							} else {
								fseek($stream,$pos);
							}
						}
						$t = microtime();
						$t = "";
						$polId = NProfiler::start("ParseOneLine".$t);
						$pole = $this->parseOneLine($lines,$parseMetaTag,$metaSingle);
						NProfiler::stop("ParseOneLine".$t,$polId);
						
						$aaaId = NProfiler::start("addArrayToArray".$t);
						$ret = $this->addArrayToArray($pole, $ret,$metaSingleA2A);
						NProfiler::stop("addArrayToArray".$t,$aaaId);
						$nextLine = true;
						break;
				}
			} else {
				$nextLine = false;
				break;
			}
//			$lineNumber++;
			
		} while ($nextLine);
		NProfiler::stop(__FUNCTION__, $fncId);
		return $ret;
	}
	
	
	/**
		Převede odpověd sherlocka na pole
	 * @param string $response cela odpoved ze sherlocka
	 * @param int $addRawData 1-pridat raw data, 0 - bez raw dat
	 * @param bool $parseMetaData rozbalit meta data - tag M
	 * @param int $metaSingle 0/1
	 * @return array Odpoved prevedena do pole
	 **/
	
	public function parseSherlockResponseToArray(string $response,bool $addRawData = false, bool $parseMetaData = false, int $metaSingle) {
		$fncId = NProfiler::start(__FUNCTION__);
		$raw = array();
		$rawBlock = array();
		$ret = array();
		$head = array();
		$cards = array();
		$foot = array();
		$fp = fopen("php://memory", 'r+');
		fputs($fp, $response);
		rewind($fp);
		$this->errorCode =  str_replace(array("\n","\r"), '', fgets($fp));
		rewind($fp);
		$block = 0;
		do {
			$pole = $this->parsePartialSherlockResponseToArray($fp,$rawBlock,$parseMetaData,$metaSingle);
			if ($block === 0) {
				$head = $pole;
				$raw[] = $rawBlock;
				$rawBlock = [];
			}
			if ($pole !== $this->emptyArray && $block > 0) {
				$foot = $pole;
				$cards[] = $pole;
				$raw[] = $rawBlock;
				$rawBlock = [];
			}
			$block ++;
		} while ($pole !== $this->emptyArray);
		fclose($fp);
		array_pop($cards);
		if ($addRawData ) {
			$ret["RAW"] = $raw;
		}
		$ret["HEAD"] = $head;
		$ret["CARDS"] = $cards;
		$ret["FOOT"] = $foot;
		NProfiler::stop(__FUNCTION__, $fncId);
		return $ret;
	}
	
	
/*
	private function mergeMetaArrays(array $add, array $to) {
		if ( isset( $add["M"]) ) {
			foreach ($add["M"] as $key => $value) {
				if ( isset($to["M"]) ) {
					if ( array_key_exists($key, $to["M"]) ) {
						array_push($to["M"][$key], $value[0]);
					} else
					{
						$to["M"][$key] = $value;
					}
				} else {
					$to["M"][$key] = $value;
				}
			}
		} else {
			$to = $this->mergeArrays($add, $to);
		}
		return $to;
	}
*/	
	
}
