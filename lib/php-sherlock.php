<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
declare(strict_types=1);

const SH_FLAG_ADD_RAW_DATA=1;
const SH_FLAG_PARSE_META_DATA=2;

/**
 * @param int $flags bitova maska parametru - bit 1 - parse meta tagy, bit 2 - přidá raw data
 */
function parse_sherlock_reply(string $host, int $port,string $query,int $flags,array &$reply) {
	$sherlockData = "";
	$addRawData = boolval( $flags & SH_FLAG_ADD_RAW_DATA );
	$parseMetaData = boolval( $flags & SH_FLAG_PARSE_META_DATA );
	get_sherlock_data($host, $port, $query, $sherlockData);
	$metaSingle = intval (ini_get('sherlock.meta_single'));
	$sp = new SherlockParser();
	$shArray = $sp->parseSherlockResponseToArray($sherlockData,$addRawData,$parseMetaData,$metaSingle);
	$reply = $shArray;
	return $sp->errorCode;
}


/**
 Ziska data z sherlocka
 * @param string $host 
 * @param int $port
 * @param string query
 * @param string reply
 * */
function get_sherlock_data(string $host,int $port,string $query,string &$reply) {
	
	$max_wait = ini_get ( "sherlock.maxwait" );
	$timeout = ini_get ( "sherlock.timeout" );
	
	$ret = 0;
	$query .= "\n";
	$out = "";
	$reply = "";

	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if ($socket === false) {
		throw new SocketCreateFailException();
	} 
	
	socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 1, 'usec' => 0));	

	$result = socket_connect($socket, $host, $port);	
	if ($result === false) {
		throw new SocketConnectFailException("Cannot connet to: $host:$port");
	}
	
	socket_write($socket, $query, strlen($query));
	
	while ($out = socket_read($socket, 2048)) {
		//echo $out;
		$reply .= $out;
	}
	socket_close($socket);
	$reply .= $out;
	return $ret;
}


class SocketCreateFailException extends Exception
{
 }
class SocketConnectFailException extends Exception
{
 }
