PHP-Sherlock
=============

Rozšíření do php o dotazování na sherlocka a jeho následné
parsování do nativních struktur php (array).

Projekt obsahuje 2 hlavní větve
master - větev ve které je finální verze projektu včetně testů
relase - větev ve které je finální verze bez testů - používá se ke klonování v rámci composeru


Rozšíření je možné připojit k projektu pomocí Composeru
Composer:
	"repositories": [
	{
		"type": "git",
		"url": "git@bitbucket.org:it-economia/php-sherlock.git"
	}],

	"require": {
		"centrum/php-sherlock": "*"
	},
